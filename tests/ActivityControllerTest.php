<?php

namespace App\Tests;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ActivityControllerTest extends WebTestCase
{
    public function testHome(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/home');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h3', 'Trouve les personnes avec qui faire du sport');
    }

	public function testNotMember(): void
	{
		$client = static::createClient();
		$crawler = $client->request('GET', '/activity/join/7');

		//redirect sur la page de création de compte
		$this->assertResponseStatusCodeSame(302);
	}

	public function testLeader(): void
	{
		$client = static::createClient();
		$userRepository = static::getContainer()->get(UserRepository::class);

		// retrieve the test user
		$testUser = $userRepository->findOneByEmail('boites@test.com');

		// simulate $testUser being logged in
		$client->loginUser($testUser);

		// test si l'utilisateur peut modifier son activité
		$client->request('GET', '/activity/7/edit');
		$this->assertResponseIsSuccessful();
	}

	public function testActivityPageForLeader(): void
	{
		$client = static::createClient();
		$userRepository = static::getContainer()->get(UserRepository::class);

		// retrieve the test user
		$testUser = $userRepository->findOneByEmail('boites@test.com');

		// simulate $testUser being logged in
		$client->loginUser($testUser);

		// test si l'utilisateur peut supprimer son activité
		$crawler = $client->request('GET', '/activity/join/7');
		$this->assertResponseIsSuccessful();
		//$crawler->assertSelectorTextContains('button', 'Supprimer');
		//$crawler->filter('button')->matches('.btn-danger');
	}

	public function testActivityPageForParticipant(): void
	{
		$client = static::createClient();
		$userRepository = static::getContainer()->get(UserRepository::class);

		// retrieve the test user
		$testUser = $userRepository->findOneByEmail('boites@test.com');

		// simulate $testUser being logged in
		$client->loginUser($testUser);

		// test si l'utilisateur peut voir les autres membres
		$client->request('GET', '/activity/join/5');
		$this->assertResponseIsSuccessful();
		$this->assertSelectorTextContains('div.participant-row', '');
	}

	public function testErrorForNoParticipant(): void
	{
		$client = static::createClient();
		$userRepository = static::getContainer()->get(UserRepository::class);

		// retrieve the test user
		$testUser = $userRepository->findOneByEmail('caro@test.com');

		// simulate $testUser being logged in
		$client->loginUser($testUser);

		// test si une erreur est renvoyée si l'utilisateur n'est pas participant
		$client->request('GET', '/activity/join/5');
		$this->assertResponseStatusCodeSame(403);
	}


}
