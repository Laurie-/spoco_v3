<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;


/**
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 * @Vich\Uploadable
 */
#[ORM\Entity(repositoryClass: UserRepository::class)]
class User implements UserInterface, PasswordAuthenticatedUserInterface, \Serializable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 180, unique: true)]
    private $email;

    #[ORM\Column(type: 'json')]
    private $roles = [];

    #[ORM\Column(type: 'string')]
    private $password;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isVerified = false;

    #[ORM\OneToMany(mappedBy: 'author', targetEntity: Review::class)]
    private $reviews;

    #[ORM\OneToMany(mappedBy: 'author', targetEntity: Incident::class)]
    private $incidents;

    #[ORM\OneToMany(mappedBy: 'author', targetEntity: Article::class)]
    private $articles;

    #[ORM\Column(type: 'string', length: 100)]
    private $nom;

    #[ORM\Column(type: 'string', length: 100)]
    private $prenom;

    #[ORM\Column(type: 'string', nullable: true)]
    private $avatar;

    /**
     * @Vich\UploadableField(mapping="user_avatar", fileNameProperty="avatar")
     */
    private $avatarFile;

    #[ORM\OneToMany(mappedBy: 'leader', targetEntity: Activity::class)]
    private $activities;

    #[ORM\ManyToMany(targetEntity: Activity::class, mappedBy: 'users')]
    private $activitiesParticipant;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private $edited_at;

    #[ORM\OneToMany(mappedBy: 'author', targetEntity: Message::class, orphanRemoval: true)]
    private $messages;

    #[ORM\ManyToMany(targetEntity: Sport::class, inversedBy: 'users')]
    private $favorite_sport;

    #[ORM\Column(type: 'string', length: 500, nullable: true)]
    private $description;

    #[ORM\Column(type: 'date')]
    private $date_naissance;

    #[ORM\Column(type: 'string', length: 50)]
    private $genre;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $commune;


    public function __construct()
    {
        $this->reviews = new ArrayCollection();
        $this->incidents = new ArrayCollection();
        $this->articles = new ArrayCollection();
        $this->activities = new ArrayCollection();
        $this->activitiesParticipant = new ArrayCollection();
        $this->messages = new ArrayCollection();
        $this->favorite_sport = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function isVerified(): bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    /**
     * @return Collection|Review[]
     */
    public function getReviews(): Collection
    {
        return $this->reviews;
    }

    public function addReview(Review $review): self
    {
        if (!$this->reviews->contains($review)) {
            $this->reviews[] = $review;
            $review->setAuthor($this);
        }

        return $this;
    }

    public function removeReview(Review $review): self
    {
        if ($this->reviews->removeElement($review)) {
            // set the owning side to null (unless already changed)
            if ($review->getAuthor() === $this) {
                $review->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Incident[]
     */
    public function getIncidents(): Collection
    {
        return $this->incidents;
    }

    public function addIncident(Incident $incident): self
    {
        if (!$this->incidents->contains($incident)) {
            $this->incidents[] = $incident;
            $incident->setAuthor($this);
        }

        return $this;
    }

    public function removeIncident(Incident $incident): self
    {
        if ($this->incidents->removeElement($incident)) {
            // set the owning side to null (unless already changed)
            if ($incident->getAuthor() === $this) {
                $incident->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Article[]
     */
    public function getArticles(): Collection
    {
        return $this->articles;
    }

    public function addArticle(Article $article): self
    {
        if (!$this->articles->contains($article)) {
            $this->articles[] = $article;
            $article->setAuthor($this);
        }

        return $this;
    }

    public function removeArticle(Article $article): self
    {
        if ($this->articles->removeElement($article)) {
            // set the owning side to null (unless already changed)
            if ($article->getAuthor() === $this) {
                $article->setAuthor(null);
            }
        }

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getAvatar()
    {
        return $this->avatar;
    }

    public function setAvatar($avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }

    public function setAvatarFile(File $avatarFile)
    {
        $this->avatarFile = $avatarFile;
        $this->edited_at = new \DateTime('now');

    }

    public function getAvatarFile()
    {
        return $this->avatarFile;
    }

    /**
     * @return Collection<int, Activity>
     */
    public function getActivities(): Collection
    {
        return $this->activities;
    }

    public function addActivity(Activity $activity): self
    {
        if (!$this->activities->contains($activity)) {
            $this->activities[] = $activity;
            $activity->setLeader($this);
        }

        return $this;
    }

    public function removeActivity(Activity $activity): self
    {
        if ($this->activities->removeElement($activity)) {
            // set the owning side to null (unless already changed)
            if ($activity->getLeader() === $this) {
                $activity->setLeader(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Activity>
     */
    public function getActivitiesParticipant(): Collection
    {
        return $this->activitiesParticipant;
    }

    public function addActivitiesParticipant(Activity $activitiesParticipant): self
    {
        if (!$this->activitiesParticipant->contains($activitiesParticipant)) {
            $this->activitiesParticipant[] = $activitiesParticipant;
            $activitiesParticipant->addUser($this);
        }

        return $this;
    }

    public function removeActivitiesParticipant(Activity $activitiesParticipant): self
    {
        if ($this->activitiesParticipant->removeElement($activitiesParticipant)) {
            $activitiesParticipant->removeUser($this);
        }

        return $this;
    }

    public function getEditedAt(): ?\DateTime
    {
        return $this->edited_at;
    }

    public function setEditedAt(?\DateTime $edited_at): self
    {
        $this->edited_at = $edited_at;

        return $this;
    }

	public function serialize() {
                                                                  
                                                                  		return serialize(array(
                                                                  			$this->id,
                                                                  			$this->email,
                                                                  			$this->password,
                                                                  		));
                                                                  
                                                                  	}

	public function unserialize($serialized) {
                                                                  
                                                                  		list (
                                                                  			$this->id,
                                                                  			$this->email,
                                                                  			$this->password,
                                                                  			) = unserialize($serialized);
                                                                  	}

    /**
     * @return Collection<int, Message>
     */
    public function getMessages(): Collection
    {
        return $this->messages;
    }

    public function addMessage(Message $message): self
    {
        if (!$this->messages->contains($message)) {
            $this->messages[] = $message;
            $message->setAuthor($this);
        }

        return $this;
    }

    public function removeMessage(Message $message): self
    {
        if ($this->messages->removeElement($message)) {
            // set the owning side to null (unless already changed)
            if ($message->getAuthor() === $this) {
                $message->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Sport>
     */
    public function getFavoriteSport(): Collection
    {
        return $this->favorite_sport;
    }

    public function addFavoriteSport(Sport $favoriteSport): self
    {
        if (!$this->favorite_sport->contains($favoriteSport)) {
            $this->favorite_sport[] = $favoriteSport;
        }

        return $this;
    }

    public function removeFavoriteSport(Sport $favoriteSport): self
    {
        $this->favorite_sport->removeElement($favoriteSport);

        return $this;
    }

	public function __toString()
                                    	{
                                    		return $this->id;
                                    	}

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDateNaissance(): ?\DateTimeInterface
    {
        return $this->date_naissance;
    }

    public function setDateNaissance(\DateTimeInterface $date_naissance): self
    {
        $this->date_naissance = $date_naissance;

        return $this;
    }

    public function getGenre(): ?string
    {
        return $this->genre;
    }

    public function setGenre(string $genre): self
    {
        $this->genre = $genre;

        return $this;
    }

    public function getCommune(): ?string
    {
        return $this->commune;
    }

    public function setCommune(?string $commune): self
    {
        $this->commune = $commune;

        return $this;
    }

	public function getAge()
	{
		$dateInterval = $this->date_naissance->diff(new \DateTime());

		return $dateInterval->y;
	}

}
