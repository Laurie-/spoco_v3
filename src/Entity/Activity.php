<?php

namespace App\Entity;

use App\Repository\ActivityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @Vich\Uploadable
 */
#[ORM\Entity(repositoryClass: ActivityRepository::class)]
class Activity
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $title;

    #[ORM\Column(type: 'string', length: 500)]
    private $details;

    #[ORM\Column(type: 'string', length: 255)]
    private $lieu;

    #[ORM\Column(type: 'integer')]
    private $nb_user_min;

    #[ORM\Column(type: 'integer')]
    private $nb_user_max;

    #[ORM\Column(type: 'string', length: 20)]
    private $level;

    #[ORM\Column(type: 'datetime')]
    private $date;

    #[ORM\Column(type: 'string', length: 2550, nullable: true)]
    private $image;

    /**
     * @Vich\UploadableField(mapping="activity_image", fileNameProperty="image")
     */
	private $imageFile;

    #[ORM\OneToOne(mappedBy: 'activity', targetEntity: Review::class, cascade: ['persist', 'remove'])]
    private $review;

    #[ORM\OneToOne(mappedBy: 'activity', targetEntity: Incident::class, cascade: ['persist', 'remove'])]
    private $incident;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'activities')]
    #[ORM\JoinColumn(nullable: false)]
    private $leader;

    #[ORM\Column(type: 'datetime')]
    private $created_at;

    #[ORM\ManyToOne(targetEntity: Sport::class, inversedBy: 'activities')]
    #[ORM\JoinColumn(nullable: false)]
    private $sport;

    #[ORM\ManyToMany(targetEntity: User::class, inversedBy: 'activitiesParticipant')]
    private $users;

    #[ORM\OneToMany(mappedBy: 'activity', targetEntity: Message::class)]
    private $messages;

    #[ORM\ManyToMany(targetEntity: User::class )]
    #[ORM\JoinTable(name: "request_participation")]
    private $requestParticipation;


    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->messages = new ArrayCollection();
        $this->requestParticipation = new ArrayCollection();
		$this->created_at = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDetails(): ?string
    {
        return $this->details;
    }

    public function setDetails(string $details): self
    {
        $this->details = $details;

        return $this;
    }

    public function getLieu(): ?string
    {
        return $this->lieu;
    }

    public function setLieu(string $lieu): self
    {
        $this->lieu = $lieu;

        return $this;
    }

    public function getNbUserMin(): ?int
    {
        return $this->nb_user_min;
    }

    public function setNbUserMin(int $nb_user_min): self
    {
        $this->nb_user_min = $nb_user_min;

        return $this;
    }

    public function getNbUserMax(): ?int
    {
        return $this->nb_user_max;
    }

    public function setNbUserMax(int $nb_user_max): self
    {
        $this->nb_user_max = $nb_user_max;

        return $this;
    }

    public function getLevel(): ?string
    {
        return $this->level;
    }

    public function setLevel(string $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function setImageFile(File $imageFile)
    {
        $this->imageFile = $imageFile;
        $this->created_at = new \DateTime('now');

    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function getReview(): ?Review
    {
        return $this->review;
    }

    public function setReview(Review $review): self
    {
        // set the owning side of the relation if necessary
        if ($review->getActivity() !== $this) {
            $review->setActivity($this);
        }

        $this->review = $review;

        return $this;
    }

    public function getIncident(): ?Incident
    {
        return $this->incident;
    }

    public function setIncident(Incident $incident): self
    {
        // set the owning side of the relation if necessary
        if ($incident->getActivity() !== $this) {
            $incident->setActivity($this);
        }

        $this->incident = $incident;

        return $this;
    }

    public function getLeader(): ?User
    {
        return $this->leader;
    }

    public function setLeader(?User $leader): self
    {
        $this->leader = $leader;

        return $this;
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTime $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getSport(): ?Sport
    {
        return $this->sport;
    }

    public function setSport(?Sport $sport): self
    {
        $this->sport = $sport;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        $this->users->removeElement($user);

        return $this;
    }

    /**
     * @return Collection<int, Message>
     */
    public function getMessages(): Collection
    {
        return $this->messages;
    }

    public function addMessage(Message $message): self
    {
        if (!$this->messages->contains($message)) {
            $this->messages[] = $message;
            $message->setActivity($this);
        }

        return $this;
    }

    public function removeMessage(Message $message): self
    {
        if ($this->messages->removeElement($message)) {
            // set the owning side to null (unless already changed)
            if ($message->getActivity() === $this) {
                $message->setActivity(null);
            }
        }

        return $this;
    }

	public function __toString()
               	{
               		return $this->id;
               	}

    /**
     * @return Collection<int, User>
     */
    public function getRequestParticipation(): Collection
    {
        return $this->requestParticipation;
    }

    public function addRequestParticipation(User $requestParticipation): self
    {
        if (!$this->requestParticipation->contains($requestParticipation)) {
            $this->requestParticipation[] = $requestParticipation;
        }

        return $this;
    }

    public function removeRequestParticipation(User $requestParticipation): self
    {
        $this->requestParticipation->removeElement($requestParticipation);

        return $this;
    }
}
