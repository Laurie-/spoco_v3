<?php

namespace App\Form;

use App\Entity\Activity;
use App\Entity\Message;
use App\Entity\User;
use Doctrine\DBAL\Types\StringType;
use phpDocumentor\Reflection\Types\Null_;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MessageFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('content', TextareaType::class, [
	            'data' => null,
	            'empty_data' => null,
	            'required' => true,
            ])
            ->add('activity', EntityType::class, [
				'class' => Activity::class,
            ])
	        ->add('author', EntityType::class,[
		        'class' => User::class,
	        ])
	        ->add('created_at', DateTimeType::class, [
	        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Message::class,
        ]);
    }
}
