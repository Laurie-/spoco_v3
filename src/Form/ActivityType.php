<?php

namespace App\Form;

use App\Entity\Activity;
use App\Entity\Sport;
use App\Entity\User;
use phpDocumentor\Reflection\Types\String_;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ActivityType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', textType::class,[
                'attr' => array(
                    'placeholder' => "Nom de l'évènement",
                    'label' => false )
            ])
            ->add('details', TextareaType::class,[
                'attr' => array(
                    'placeholder' => "Description",
                    'label' => false )
            ])
            ->add('lieu', textType::class,[
                'attr' => array(
                    'placeholder' => "Lieu de l'évènement",
                    'label' => false )
            ])
            ->add('sport', EntityType::class,[
                'class' => Sport::class,
	            'placeholder' => 'Selectionner un sport',
                'choice_label' => 'title',
                'attr' => array(
	                'class' => 'form-select'
                )
            ])
            ->add('nb_user_min', IntegerType::class, [
				'data' => 1,
                'attr' => array(
                    'placeholder' => '1'
                )
            ])
            ->add('nb_user_max', IntegerType::class, [
				'data' => 3,
                'attr' => array(
                    'placeholder' => '10'
                )
            ])
            ->add('level', ChoiceType::class,[
	            'placeholder' => 'Selectionner un niveau',
	            'attr' => array(
		            'class' => 'form-select'),
                'choices' => array(
                    'Débutant'    => 'Débutant',
                    'Intermédiaire'=> 'Intermédiaire',
                    'Confirmé'      => 'Confirmé')
            ])
            ->add('date', DateTimeType::class, [
	            'attr' => array(
		            'class' => 'datepicker form-select ',
		            'placeholder' => 'Choisir une date'
	            ),
                'input' => 'datetime',
                'widget' => 'single_text',
	            'html5' => false,
            ])
            ->add('imageFile', vichImageType::class,[
                'required'=> false,
	            'allow_delete' => false,
	            'image_uri' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Activity::class,
        ]);
    }
}
