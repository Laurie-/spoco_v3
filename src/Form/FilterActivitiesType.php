<?php

namespace App\Form;

use App\Entity\Sport;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FilterActivitiesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
	        ->add('sport', EntityType::class,[
		        'class' => Sport::class,
		        'placeholder' => 'Selectionner un sport',
		        'choice_label' => 'title',
		        'attr' => array(
			        'placeholder' => 'Choisir quel sport',
			        'class' => 'form-select form-select-sm'
		        )
	        ])
	        ->add('level', ChoiceType::class,[
		        'placeholder' => 'Selectionner un niveau',
				'attr' => ['class' => 'form-select form-select-sm'],
		        'choices' => array(
			        'Débutant'    => 'Débutant',
			        'Intermédiaire'=> 'Intermédiaire',
			        'Expert'      => 'Expert')
	        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
