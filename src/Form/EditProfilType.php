<?php

namespace App\Form;

use App\Entity\Sport;
use App\Entity\User;
use App\Repository\SportRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class EditProfilType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
		$editedObject = $builder->getData();
        $builder
            //->add('email')
            //->add('password')
	        ->add('nom', textType::class,
		        [
			        'attr' => ['placeholder' => 'Nouveau nom',
				        'require' => false]
		        ])
	        ->add('prenom', textType::class,
		        [
			        'attr' => ['placeholder' => 'Nouveau prénom',
				        'require' => false]
		        ])
	        ->add('description', TextareaType::class,
		        [
			        'attr' => ['placeholder' => 'Entrez une description de présentation',
				        'require' => false]
		        ])
	        ->add('avatarFile', vichImageType::class , [
		        'download_link' => false,
		        'allow_delete' => false,
		        'image_uri' => false,
				'required' => false,
				'attr' => [ 'required' => false ]
		        ])

	        ->add('favorite_sport', EntityType::class, [
		        'class' => Sport::class,
		        'choice_label' => 'title',
		        'multiple' => true,
		        'expanded' => true,
		        'mapped' => true,
		        'by_reference' => false,
		        'query_builder' => function(SportRepository $repo) {
			        return $repo->createSportsQueryBuilder();
		        },
	        ])
	        ->add('commune', textType::class,[
		        'attr' => array(
			        'placeholder' => "Votre commune",
			        'label' => false )
	        ])
	        ->add('date_naissance', DateType::class, [
		        'attr' => array(
			        'class' => 'datepicker-age',
			        'placeholder' => "Votre date de naissance",
			        'label' => false ),
		        'input' => 'datetime',
		        'widget' => 'single_text',
		        'html5' => false,
		        'format' => 'dd-MM-yyyy',
	        ])
	        ->add('genre', ChoiceType::class,[
		        'choices' => array(
			        'Une femme'    => 'femme',
			        'Un homme'=> 'homme',
			        'Non binaire' => 'nb'),
		        'multiple' => false,
		        'expanded' => true,
		        'attr' => [
			        'label' => 'Vous êtes :'
		        ]
	        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
	        'csrf_protection' => true,
        ]);
    }
}
