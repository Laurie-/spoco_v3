<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Vich\UploaderBundle\Form\Type\VichImageType;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom', textType::class,[
	            'attr' => array(
		            'placeholder' => "Votre nom",
		            'label' => false )
            ])
            ->add('prenom', textType::class,[
	            'attr' => array(
		            'placeholder' => "Votre prénom",
		            'label' => false )
            ])

	        ->add('email', RepeatedType::class, [
		        'type' => EmailType::class,
		        'invalid_message' => 'Les emails doivent êtres valides',
		        //'options' => ['attr' => ['class' => 'password-field']],
		        'required' => true,
		        'first_options'  => ['label' => 'Votre email'],
		        'second_options' => ['label' => 'Confirmez votre email'],
		        'constraints' => [
			        new NotBlank([
				        'message' => 'Merci de renseigner un email',
			        ]),
		        ]
	        ])
	        ->add('genre', ChoiceType::class,[
		        'choices' => array(
			        'Une femme'    => 'femme',
			        'Un homme'=> 'homme',
			        'Non binaire' => 'nb'),
				'multiple' => false,
				'expanded' => true,
		        'attr' => [
					'label' => 'Vous êtes :'
    ]
	        ])
	        ->add('date_naissance', DateType::class, [
		        'attr' => array(
			        'class' => 'datepicker-age',
			        'placeholder' => "Votre date de naissance",
			        'label' => false ),
		        'input' => 'datetime',
                'widget' => 'single_text',
	            'html5' => false,
		        'format' => 'dd-MM-yyyy',
	        ])
            ->add('avatarFile', vichImageType::class,[
	            'attr' => array(
		            'label' => false )
            ])
	        ->add('commune', textType::class,[
		        'attr' => array(
			        'placeholder' => "Votre commune",
			        'label' => false )
	        ])
            ->add('agreeTerms', CheckboxType::class, [
                'mapped' => false,
	            'label' => "J'accepte les CGU",
                'constraints' => [
                    new IsTrue([
                        'message' => 'Vous devez accepter les CGU pour valider votre inscription',
                    ]),
                ],
            ])
	        ->add('password', RepeatedType::class, [
		        'type' => PasswordType::class,
		        'invalid_message' => 'The password fields must match.',
		        'options' => ['attr' => ['class' => 'password-field']],
		        'required' => true,
		        'first_options'  => ['label' => 'Votre mot de passe'],
		        'second_options' => ['label' => 'Répeter le mot de passe'],
		        'attr' => [
					'class' => 'col-md-6'
		        ],
		        'constraints' => [
	                new NotBlank([
		                'message' => 'Merci de renseigner un mot de passe',
	                    ]),
			        ]
	        ])
            /*->add('plainPassword', passwordType::class, [
                //'type' => passwordType::class,
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'mapped' => false,
                'attr' => ['placeholder' => "Votre mot de passe",'autocomplete' => 'new-password'],
                //'first_options'  => ['label' => 'Mot de passe'],
                //'second_options' => ['label' => 'Confirmer le mot de passe'],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a password',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Your password should be at least {{ limit }} characters',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ],
            ])*/
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
