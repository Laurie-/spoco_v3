<?php

namespace App\Repository;

use App\Entity\Activity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use phpDocumentor\Reflection\Types\Collection;

/**
 * @method Activity|null find($id, $lockMode = null, $lockVersion = null)
 * @method Activity|null findOneBy(array $criteria, array $orderBy = null)
 * @method Activity[]    findAll()
 * @method Activity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ActivityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Activity::class);
    }

    public function findBySomeCriteria($sport, $level): ?Array
    {
        $qb= $this->createQueryBuilder('a')
            ->andWhere('a.sport = :vals')
	        ->andWhere('a.level = :vall')
            ->setParameter('vals', $sport)
	        ->setParameter('vall', $level);
	        return $qb->getQuery()->getResult();
    }

	public function activityHome(): ?Array
	{
		$qb= $this->createQueryBuilder('a')
			->orderBy('a.date', 'DESC')
			->setMaxResults(4);
		return $qb->getQuery()->getResult();
	}

	// /**
	//  * @return Activity[] Returns an array of Activity objects
	//  */
	/*
	public function findByExampleField($value)
	{
		return $this->createQueryBuilder('a')
			->andWhere('a.exampleField = :val')
			->setParameter('val', $value)
			->orderBy('a.id', 'ASC')
			->setMaxResults(10)
			->getQuery()
			->getResult()
		;
	}
	*/




}
