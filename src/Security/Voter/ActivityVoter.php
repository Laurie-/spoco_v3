<?php

namespace App\Security\Voter;

use App\Entity\Activity;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class ActivityVoter extends Voter
{
	private $security;
	public function __construct(Security $security)
	{
		$this->security = $security;
	}

    protected function supports(string $attribute, $subject): bool
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, ["LEADER", "PARTICIPANT"])
            && $subject instanceof \App\Entity\Activity;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
	    /** @var Activity $subject */
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }
        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
	        case 'LEADER':
		        if ($subject->getLeader() == $user) {
			        return true;
		        }
		        break;
            case 'PARTICIPANT':
	            foreach($subject->getUsers() as $participant){
					if($participant->getId() === $user->getId()){
						return true;
					}
	            }
        }
        return false;
    }
}
