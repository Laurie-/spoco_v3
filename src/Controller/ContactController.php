<?php

namespace App\Controller;

use App\Form\ContactType;
use App\Services\mailerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends AbstractController
{
    #[Route('/contact', name: 'app_contact')]
    public function index(Request $request, mailerService $mailer)
    {
	    $form = $this->createForm(ContactType::class);
	    $form->handleRequest($request);

	    if ($form->isSubmitted() && $form->isValid()) {
		    $contactFormData = $form->getData();
			$from = $contactFormData['mail'];
		    $subject = 'Demande de contact sur votre site de ' . $contactFormData['mail'];
		    $content = $contactFormData['prenom'] .' '. $contactFormData['nom'] . ' vous a envoyé le message suivant: ' . $contactFormData['message'];
		    $mailer->sendEmail(subject: $subject, content: $content);
		    $this->addFlash('success', 'Votre message a été envoyé');
		    return $this->redirect($request->server->get('HTTP_REFERER'));
	    }
	    return $this->render('contact/index.html.twig', [
		    'our_form' => $form->createView()
	    ]);
    }
}


