<?php

namespace App\Controller;

use App\Entity\Sport;
use App\Entity\User;
use App\Form\EditProfilType;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Session\Session;



class ProfilController extends AbstractController
{
    private $userRepository;
    private $entityManager;

    public function __construct(UserRepository $userRepository, EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->userRepository = $userRepository;
    }


    #[Route('/profil/{id}', name: 'app_profil')]
    public function index($id): Response
    {
        $user = $this->userRepository->find($id);
        $activities = $user->getActivities();
		$activitiesParticipation = $user->getActivitiesParticipant();
        return $this->render('profil/index.html.twig', [
            'controller_name' => 'ProfilController',
            'user' => $user,
            'activities' => $activities,
	        'activitiesParticipation' => $activitiesParticipation
        ]);
    }

	#[Route('/profil/edit/{id}', name: 'app_profil_edit', methods: ['GET', 'POST'])]
	public function edit(int $id, User $user, Request $request, EntityManagerInterface $entityManager, Session $session): Response
	{
		$utilisateur = $this->getUser();
		if($utilisateur->getId() != $id )
		{
			// un utilisateur ne peut pas en modifier un autre
			//$session->set("message", "Vous ne pouvez pas modifier cet utilisateur");
			$this->addFlash('success', ' "Vous ne pouvez pas modifier cet utilisateur"');
			return $this->redirectToRoute('app_profil', ["id"=> $user->getId()]);
		}
		//$user = $this->userRepository->find($id);
		$form = $this->createForm(EditProfilType::class, $user);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$this->entityManager->persist($user);
			$this->entityManager->flush();
			return $this->redirectToRoute('app_profil', ["id"=> $user->getId()], Response::HTTP_SEE_OTHER);
		}

		return $this->render('profil/edit.html.twig', [
			'user' => $user,
			'editForm' => $form->createView(),
		]);

	}

	#[Route('/profil/delete/{id}', name: 'user_delete', methods: ['POST'])]
	public function delete(Request $request, User $user, EntityManagerInterface $entityManager): Response
	{
		if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
			$entityManager->remove($user);
			$entityManager->flush();
		}

		return $this->redirectToRoute('activity_index', [], Response::HTTP_SEE_OTHER);
	}

}
