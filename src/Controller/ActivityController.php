<?php

namespace App\Controller;

use App\Entity\Activity;
use App\Entity\Message;
use App\Entity\User;
use App\Form\ActivityType;
use App\Form\FilterActivitiesType;
use App\Form\MessageFormType;
use App\Repository\ActivityRepository;
use App\Repository\UserRepository;
use App\Services\mailerService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/activity')]
class ActivityController extends AbstractController
{
    #[Route('/', name: 'activity_index', methods: ['GET', 'POST'])]
    public function index(ActivityRepository $activityRepository, Request $request): Response
    {
		$form = $this->createForm(filterActivitiesType::class);
	    $form->handleRequest($request);

	    if ($form->isSubmitted() && $form->isValid()) {
		    $datas = $form->getData();
			$sport = $datas["sport"]->getId();
			$level = $datas["level"];
		    return $this->render('activity/index.html.twig', [
			    'activities' => $activityRepository->findBySomeCriteria($sport, $level),
			    'form' => $form->createView()
		    ]);
	    }
	    return $this->render('activity/index.html.twig', [
		    'activities' => $activityRepository->findAll(),
		    'form' => $form->createView()
	    ]);

    }

    #[Route('/new', name: 'activity_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $activity = new Activity();
        $form = $this->createForm(ActivityType::class, $activity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $activity->setLeader($this->getUser());
                $entityManager->persist($activity);
                $entityManager->flush();
                return $this->redirectToRoute('activity_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('activity/new.html.twig', [
            'activity' => $activity,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'activity_show', methods: ['GET'])]
    public function show(Activity $activity): Response
    {
		$user = $this->getUser();

	    if(!$user) {
			return $this->redirectToRoute('app_register');
	    }
		$isParticipant = false;

		foreach($activity->getUsers() as $participant){
			if($participant === $user) {
				$isParticipant = true;
				break;
			} else {
				$isParticipant = false;
			}
		}
		if ($isParticipant) {
			$this->denyAccessUnlessGranted('PARTICIPANT', $activity);
			return $this->redirectToRoute('activity_join', ["id" => $activity->getId()], Response::HTTP_SEE_OTHER);
		} elseif ($user === $activity->getLeader()) {
			$this->denyAccessUnlessGranted('LEADER', $activity);
			return $this->redirectToRoute('activity_join', ["id" => $activity->getId()], Response::HTTP_SEE_OTHER);
		} else {
			return $this->render('activity/show.html.twig', [
				'activity' => $activity,
			]);
		}
    }

    #[Route('/{id}/edit', name: 'activity_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Activity $activity, EntityManagerInterface $entityManager): Response
    {
	    $this->denyAccessUnlessGranted('LEADER', $activity);
        $form = $this->createForm(ActivityType::class, $activity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('activity_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('activity/edit.html.twig', [
            'activity' => $activity,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'activity_delete', methods: ['POST'])]
    public function delete(Request $request, Activity $activity, EntityManagerInterface $entityManager): Response
    {
	    $this->denyAccessUnlessGranted('LEADER', $activity);
        if ($this->isCsrfTokenValid('delete'.$activity->getId(), $request->request->get('_token'))) {
            $entityManager->remove($activity);
            $entityManager->flush();
        }

        return $this->redirectToRoute('activity_index', [], Response::HTTP_SEE_OTHER);
    }

	#[Route('/join/{id}', name: 'activity_join', methods: ['GET', 'POST'])]
	public function joinActivity(Request $request, Activity $activity, EntityManagerInterface $entityManager,User $userCurrent, $id, mailerService $mailer): Response
	{

		$nbPartMax = $activity->getNbUserMax();
		$nbPartCurrent = $activity->getUsers();
		$user = $this->getUser();
		$leader = $activity->getLeader();

		if($user === $leader) {
			$this->denyAccessUnlessGranted('LEADER', $activity);
		} elseif($user) {
			$this->denyAccessUnlessGranted('PARTICIPANT', $activity);
		}

		//Si le leader accepte la demande :
		if ((count($nbPartCurrent) < $nbPartMax) && ($user !== $leader)) {
			$activity->addUser($user);
			$entityManager->persist($user);
			$entityManager->persist($activity);
			$entityManager->flush();
		}

		$messages = $activity->getMessages();

		$mess = new Message();
		$form = $this->createForm(MessageFormType::class, $mess);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$mess->setAuthor($user);
			$mess->setActivity($activity);
			$entityManager->persist($mess);
			$entityManager->flush();
			return $this->redirect($request->getUri());
		}

		if(!$user) {
			return $this->redirectToRoute('app_register');
		}

		return $this->renderForm('activity/show.html.twig', [
			'activity' => $activity,
			'form' => $form,
			'messages' => $messages,
		]);

	}

	#[Route('/request/{id}', name: 'activity_request', methods: ['GET', 'POST'])]
	public function requestActivity(Request $request, Activity $activity, EntityManagerInterface $entityManager, $id, mailerService $mailer): Response
	{
		$nbPartMax = $activity->getNbUserMax();
		$nbPartCurrent = $activity->getUsers();
		$leader = $activity->getLeader();
		$user = $this->getUser();

		if ((count($nbPartCurrent) < $nbPartMax) && ($user !== $leader)) {
			$activity->addRequestParticipation($user);
			$entityManager->persist($user);
			$entityManager->persist($activity);
			$entityManager->flush();
		}

		//envoi demande de participation au leader

		$activityId = $activity->getId();
		$hrefLeader = "http://127.0.0.1:8000/activity/join/".$activityId;

		$subject = 'Demande de participation à votre activité ';
		$content = '<h2>Bonjour '. $leader->getPrenom() .', </h2></br>
		Vous avez une demande de participation de la part de '.$user->getPrenom().' '.$user->getNom().'</br> 
		pour votre activité : ' . $activity->getTitle().'</br>
		Merci de lui envoyer votre réponse <a href="'.$hrefLeader.'">ici</a> :)';
		$mailer->sendEmail(to:$leader->getEmail(), subject: $subject, content: $content);

		//$this->addFlash('success', 'Votre demande de participation a bien été envoyée');

		return $this->renderForm('activity/show.html.twig', [
			'activity' => $activity,
			'requestParticipant' => $activity->getRequestParticipation()
		]);

	}

	#[Route('/join/{id}/{user}', name: 'user_accept', methods: ['GET', 'POST'])]
	public function acceptActivity(Request $request, Activity $activity, EntityManagerInterface $entityManager, mailerService $mailer, UserRepository $userRepository, $user): Response
	{
		$leader = $activity->getLeader();
		$user = $userRepository->find($user);
		$nbPartMax = $activity->getNbUserMax();
		$nbPartCurrent = $activity->getUsers();

		//ajout du participant au groupe + suppression de la liste de demande
		if ((count($nbPartCurrent) < $nbPartMax) && ($user !== $leader)) {
			$activity->removeRequestParticipation($user);
			$activity->addUser($user);
			$entityManager->persist($user);
			$entityManager->persist($activity);
			$entityManager->flush();
		}

		//Envoi du mail au participant
		$activityId = $activity->getId();
		$hrefParticipant = "http://127.0.0.1:8000/activity/join/".$activityId;

		$subject = 'Votre demande a été acceptée ! ';
		$content = '<h2>Bonjour '. $user->getPrenom() .', </h2></br>
		Votre demande de participation pour '.$activity->getTitle().' à été acceptée</br> 
		Vous avez maintenant accès à la page du groupe pour discuter avec les autres membres :  <a href="'.$hrefParticipant.'">ici </a> :)';

		$mailer->sendEmail(to: $user->getEmail(), subject: $subject, content: $content );

		$this->addFlash('success', 'Votre réponse de participation a bien été envoyée');


		$mess = new Message();
		$form = $this->createForm(MessageFormType::class, $mess);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$mess->setAuthor($user);
			$mess->setActivity($activity);
			$entityManager->persist($mess);
			$entityManager->flush();
			return $this->redirect($request->getUri());
		}
		return $this->redirectToRoute('activity_join' ,["id" => $activity->getId()]);

	}

	#[Route('/reject/{id}/{user}', name: 'user_reject', methods: ['GET', 'POST'])]
	public function rejectActivity(Request $request, Activity $activity, EntityManagerInterface $entityManager, $id, mailerService $mailer, UserRepository $userRepository, $user): Response
	{
		$user = $userRepository->find($user);

		//suppression de la liste de demande
		$activity->removeRequestParticipation($user);
		$entityManager->persist($user);
		$entityManager->persist($activity);
		$entityManager->flush();

		//Envoi du mail au participant
		$hrefParticipant = "http://127.0.0.1:8000/activity/";

		$subject = 'Votre demande a été refusée... ';
		$content = '<h2>Bonjour '. $user->getPrenom() .', </h2></br>
		Votre demande de participation pour '.$activity->getTitle().' à été refusée</br> 
		Vous pouvez peut être (re)trouver votre bonheur :  <a href="'.$hrefParticipant.'">ici </a> :)';

		$mailer->sendEmail(to: $user->getEmail(), subject: $subject, content: $content );

		$this->addFlash('success', 'Votre refus de demande à bien été envoyée');

		return $this->redirectToRoute('activity_show' ,["id" => $activity->getId()]);

	}
}