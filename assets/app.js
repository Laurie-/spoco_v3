/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.scss'

// start the Stimulus application
import './bootstrap';
import flatpickr from "flatpickr";
import 'flatpickr/dist/flatpickr.css';
require('@fortawesome/fontawesome-free/css/all.min.css');
require('@fortawesome/fontawesome-free/js/all.js');
require('bootstrap');


flatpickr(".datepicker", {
    enableTime: true,
    dateFormat: "Y-m-d H:i",
});

flatpickr(".datepicker-age", {
    //enableTime: true,
    dateFormat: "d-m-Y",
});

$('.carousel').carousel({
    interval: 100
})

const collapseElementList = document.querySelectorAll('.collapse')
const collapseList = [...collapseElementList].map(collapseEl => new bootstrap.Collapse(collapseEl))

